# -*- coding: utf-8 -*-

import uno
import unohelper
from com.sun.star.awt import XKeyHandler
from com.sun.star.beans import PropertyValue

key_handler = None
harakat = ['ّ', 'ٌ', 'ُ', 'ً', 'َ', 'ِ', 'ٍ', 'ْ']
colors = [1400606,9498256,9109504,16764107,139,11393254,16753920,6950317]
colors_dict = {k: v for k, v in zip(harakat, colors)}


def RGB(R,G,B):
    packed = int('%02x%02x%02x' % (R, G, B), 16)
    return packed


def hex_to_rgb(hex_color):
    return tuple(int(hex_color.strip("#")[i:i+2], 16) for i in (0, 2, 4))


class KeyHandler(unohelper.Base, XKeyHandler):
    TAB = 1282
    CUT = 1297
    DELETE = 1286

    def __init__(self, ctx, doc):
        self.ctx = ctx
        self.doc = doc

    def recentColor(self):
        props = (
            PropertyValue(Name='nodepath', Value="/org.openoffice.Office.Common/UserColors/"),
            PropertyValue(Name="enableasync", Value=False)
                )
        configurationProvider = self.ctx.ServiceManager.createInstanceWithContext("com.sun.star.configuration.ConfigurationProvider", self.ctx)
        usercolors = configurationProvider.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", props)
        return usercolors.getByName("RecentColor")[0] or -1

    def keyPressed(self, keyevent):
        BACKSPACE = 1283
        # harakat = ['ّ', 'ٌ', 'ُ', 'ً', 'َ', 'ِ', 'ٍ', 'ْ']
        # RED = 15926533
        # BLACK = 197379
        views = self.doc.CurrentController.getViewCursor()
        cursor = views.Text.createTextCursorByRange(views)
        cursor.CharColor = self.recentColor()
        if keyevent.Modifiers <= 1 and keyevent.KeyFunc == 0:
            if keyevent.KeyCode == BACKSPACE:
                cursor.goLeft(1, True)
                cursor.CharColor = self.recentColor()
            if keyevent.KeyChar.value in harakat:
                cursor.setPropertyValue('CharColor', colors_dict.get(keyevent.KeyChar.value, self.recentColor()))
                # cursor.setPropertyValue('CharColor', RED)
        return False
    def keyReleased(self, keyevent): return False
    def disposing(self, eventobject): pass


def live_coloring():
    global key_handler
    doc = XSCRIPTCONTEXT.getDocument()
    ctx = XSCRIPTCONTEXT.getComponentContext()
    controller = doc.getCurrentController()
    key_handler = KeyHandler(ctx, doc)
    controller.addKeyHandler(key_handler)


def stop_live_coloring():
    global key_handler
    doc = XSCRIPTCONTEXT.getDocument()
    ctx = XSCRIPTCONTEXT.getComponentContext()
    controller = doc.getCurrentController()
    controller.removeKeyHandler(key_handler)


def after():
    doc = XSCRIPTCONTEXT.getDocument()
    letters = "[ا-ي]|[ﻻ|ﻵ|ﻷ|ﻹ|ة|ئ|ء|ؤ|ى|آ|أ|إ]"
    numbers = "۰|۱|۲|۳|٤|٥|٦|٧|٨|٩"
    # harakat = "ّ|ٌ|ُ|ً|َ|ِ|ٍ|ْ"
    controller = doc.CurrentController
    # view_cursor = doc.CurrentController.ViewCursor
    search_descriptor = doc.createSearchDescriptor()
    search_descriptor.SearchString = "|".join(harakat)
    search_descriptor.SearchRegularExpression = True
    if controller.Selection.supportsService("com.sun.star.text.TextTableCursor"):
        table = doc.getTextTables().getByName(controller.ViewCursor.TextTable.Name)
        rangeName = doc.CurrentController.Selection.getRangeName()
        for i in range(int(rangeName[1]), int(rangeName[4]) + 1):
            for x in range(ord(rangeName[0]), ord(rangeName[3]) + 1):
                cell = table.getCellByName(f'{chr(x)}{i}')
                start = cell.Text.createTextCursorByRange(cell.Start)
                end = cell.Text.createTextCursorByRange(cell.End)
                if doc.Text.compareRegionEnds(start, end) >= 0:
                    start.goRight(0, False)
                    found = doc.findNext(start, search_descriptor)
                    while found:
                        if doc.Text.compareRegionEnds(found, end) == -1:
                            break
                        found.CharColor = colors_dict.get(found.String, -1)
                        found = doc.findNext(found.End, search_descriptor)
    if controller.Selection.supportsService("com.sun.star.text.TextRanges"):
        selection = doc.getCurrentController().getSelection().getByIndex(0)
        start = selection.Text.createTextCursorByRange(selection.Start)
        end = selection.Text.createTextCursorByRange(selection.End)
        if doc.Text.compareRegionEnds(start, end) >= 0:
            start.goRight(0, False)
            found = doc.findNext(start, search_descriptor)
            while found:
                if doc.Text.compareRegionEnds(found, end) == -1:
                    break
                found.CharColor = colors_dict.get(found.String, -1)
                found = doc.findNext(found.End, search_descriptor)


g_exportedScripts = live_coloring, stop_live_coloring, after
